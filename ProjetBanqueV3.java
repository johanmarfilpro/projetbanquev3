import java.util.Scanner;
import java.util.Random;
import java.io.*;
import java.nio.file.*;
public class ProjetBanqueV3{
	
	public final static int ID_AGENCES=0;
	public final static int NOM_AGENCES=1;
	public final static int ADRESSE_AGENCES=2;
	
	public final static int ID_COMPTES=0;
	public final static int CODE_AGENCES_COMPTES=1;
	public final static int ID_CLIENTS_COMPTES=2;
	public final static int TYPE_COMPTES=3;
	public final static int SOLDE_COMPTES=4;
	public final static int DECOUVERT_COMPTES=5;
	
	public static void main(String args[]) throws Exception,IOException{
		
		
		String[][] agences = new String[3][0];
		Path agencePath = Path.of("./csv/agences.csv");
		File fichierAgences = agencePath.toFile();
		if(fichierAgences.isFile()){
			agences = pushCsvToTab(fichierAgences,3);
		} else{
			agences = initFileCsvAgence(fichierAgences,agences);
		}
		
		String[][] clients = new String [5][0];
		Path clientsPath = Path.of("./csv/clients.csv");
		File fichierClients = clientsPath.toFile();
		if(fichierClients.isFile()){
			clients = pushCsvToTab(fichierClients,5);
		} else{
			clients = initFileCsvClient(fichierClients,clients);
		}
		
		String[][] comptes = new String[6][0];
		Path comptePath = Path.of("./csv/comptes.csv");
		File fichierComptes = comptePath.toFile();
		if(fichierComptes.isFile()){
			comptes = pushCsvToTab(fichierComptes,6);
		} else{
			comptes = initFileCsvCompte(fichierComptes,comptes);
		}
		
		Scanner sc = new Scanner(System.in);
		Random rand = new Random();
		String choix = "";
		
		do{
			System.out.println("A- Creation");
			System.out.println("B- Modification");
			System.out.println("C- Suppression");
			System.out.println("D- Affichage");
			System.out.println("E- Recherche");
			System.out.println("F- Afficher la liste des comptes d_un client");
			System.out.println("G- Imprimer les information d_un client");
			System.out.println("H- Quitter le programme");
			choix = sc.nextLine();
			
			switch(choix){
				case "A":
					String choixA = "";
					do{
						System.out.println("A- Creation agence");
						System.out.println("B- Creation client");
						System.out.println("C- Creation compte bancaire");
						System.out.println("R- Retour");
						choixA = sc.nextLine();
						switch(choixA){
							case "A":
								
								agences = askAgence(sc,agences,rand);
								pushToCsv(agences,fichierAgences);
								break;
							case "B":
								clients = createClient(clients,sc,rand);
								pushToCsv(clients,fichierClients);
								break;
							case "C":
								comptes = askCompte(sc,comptes,agences,rand,clients);
								pushToCsv(comptes, fichierComptes);
								break;
							case "R":
								break;
							default:
								System.out.println("ERREUR : le menu demander n_existe pas");
						}
					}while(!(choixA.toUpperCase().equals("R")));
					break;
				case "B":
					String choixB = "";
					do{
						System.out.println("A- Modification agence");
						System.out.println("B- Modification client");
						System.out.println("C- Modification compte bancaire");
						System.out.println("R- Retour");
						choixB = sc.nextLine();
						switch(choixB){
							case "A":
								agences = modifAgenceGlobal(agences,sc);
								pushToCsv(agences,fichierAgences);
								break;
						case "B":
								clients = modifClientsGlobal(clients,sc);
								pushToCsv(clients,fichierClients);
								break;	
							case "C":
								comptes = modifCompteGlobal(comptes,sc);
								pushToCsv(comptes, fichierComptes);
								break;
							case "R":
								break;
							default:
								System.out.println("ERREUR : le menu demander n_existe pas");
						}
					}while(!(choixB.toUpperCase().equals("R")));
					break;
				case "C":
					String choixC = "";
					do{
						System.out.println("A- Suppression agence");
						System.out.println("B- Suppression client");
						System.out.println("C- Suppression compte bancaire");
						System.out.println("R- Retour");
						choixC = sc.nextLine();
						switch(choixC){
							case "A":
								String codeAgenceSupp;
								int indexSuppA = -1;
								do{
									System.out.println("Veuillez saisir l_id de l_agence a supprimmer");
									codeAgenceSupp = sc.nextLine().trim();
									for(int i=0;i<agences[0].length;i++){
										if(codeAgenceSupp.equals(agences[0][i])){
											indexSuppA = i;
										}
									}
									if(indexSuppA < 0){
										System.out.println("Cette agence n_existe pas (veuillez saisir un code valable)");
									}
									if(indexSuppA == 0){
										System.out.println("Vous ne pouvez pas supprimer l_agence generique");
									}									
								}while(indexSuppA<0 ||indexSuppA == 0);
								agences = deleteAgence(agences,sc,comptes,codeAgenceSupp,indexSuppA);
								pushToCsv(agences,fichierAgences);
								comptes =modifCompte(comptes,codeAgenceSupp);
								pushToCsv(comptes, fichierComptes);
								break;
							case "B":
								String codeClientSupp;
								int indexSupp = -1;
								do{
									System.out.println("Veuillez saisir l_id du clients a supprimmer");
									codeClientSupp = sc.nextLine();
									for(int i=0;i<clients[0].length;i++){
										if(codeClientSupp.equals(clients[0][i])){
											indexSupp = i;
										}
									}
									if(indexSupp < 0){
										System.out.println("Ce compte client n_existe pas (veuillez saisir un code valable)");
									}									
								}while(indexSupp<0);
								clients = deleteClient(clients,sc,codeClientSupp,indexSupp);
								pushToCsv(clients, fichierClients);
								comptes = deleteCompteOfClient(comptes,sc,clients,codeClientSupp);
								pushToCsv(comptes, fichierComptes);
								break;
							case "C":
							comptes = deleteCompte(comptes,sc);
							pushToCsv(comptes, fichierComptes);
								break;
							case "R":
								break;
							default:
								System.out.println("ERREUR : le menu demander n_existe pas");
						}
					}while(!(choixC.toUpperCase().equals("R")));
					break;
				case "D":
					String choixD = "";
					do{
						System.out.println("A- Affichage toutes les agence");
						System.out.println("B- Affichage tous les clients");
						System.out.println("C- Affichage tous les compte bancaire");
						System.out.println("R- Retour");
						choixD = sc.nextLine();
						switch(choixD){
							case "A":
								displayAgences(sc,agences);
								break;
							case "B":
								displayClient(clients);
								break;
							case "C":
								displayComptes(sc,comptes);
								break;
							case "R":
								break;
							default:
								System.out.println("ERREUR : le menu demander n_existe pas");
						}
					}while(!(choixD.toUpperCase().equals("R")));
					break;
				case "E":
					String choixE = "";
					do{
						System.out.println("A- Recherche compte");
						System.out.println("B- Recherche client");
						System.out.println("R- Retour");
						choixE = sc.nextLine();
						switch(choixE){
							case "A":
								rechercheCompte(sc,comptes);
								break;
							case "B":
								searchClient(clients,sc);
								break;
							case "R":
								break;
							default:
								System.out.println("ERREUR : le menu demander n_existe pas");
						}
					}while(!(choixE.toUpperCase().equals("R")));
					break;
				case "F":
					System.out.println("saisir le code client");
					String idClient = sc.nextLine().trim();
					displayComptes(sc,comptes,idClient);
					break;
				case "G":
					printClient(clients,comptes,sc);
					break;
				case "H":
					break;
				default:
					System.out.println("ERREUR : le menu demander n_existe pas");
			}
			
		}while(!(choix.toUpperCase().equals("H")));
		
		

		
		
	}
	/**
	*cette méthode permet la modification d'un clients;
	*
	*/
	public static String[][] modifClientsGlobal(String[][] clients,Scanner sc){
		
	 String codeClientModifier= "";

		int indexModifier=-1;
		do{
			System.out.println("Veuillez saisir l_id du clients a modifier");
			codeClientModifier = sc.nextLine();
			
			for(int i=0;i<clients[0].length;i++){
				if(codeClientModifier.equals(clients[0][i])){
					indexModifier = i;
					break;
				}
			}
			if(indexModifier < 0){
				System.out.println("Ce compte client n_existe pas (veuillez saisir un code valable)");
			}									
		}while(indexModifier<0);
		
		
		
		
		String modifNom;
		do {
			System.out.print("Voulez vous modifier le nom ? oui ('O') ou non ('N') : ");
				modifNom = sc.nextLine();
			
			
			if (modifNom.equals("O")){
			
				System.out.println("Saisissir la valeur a remplacer :");
				String nomRemplace = sc.nextLine();
				
				for (int i = 0;i<clients[0].length;i++){
			
				if (clients [0][i].equals(codeClientModifier)){
				clients[1][i] = nomRemplace;
			}
				}
			}
			}while(!(modifNom.equals("O")) && (!(modifNom.equals("N"))));

		 String modifPrenom;
		do {
			System.out.print("Voulez vous modifier le prenom ? oui ('O') ou non ('N') : ");
				modifPrenom = sc.nextLine();
			
			
			if (modifPrenom.equals("O")){
			
				System.out.println("Saisissir la valeur a remplacer :");
				String prenomRemplace = sc.nextLine();
				
				for (int i = 0;i<clients[0].length;i++){
			
				if (clients [0][i].equals(codeClientModifier)){
				clients[2][i] = prenomRemplace;
				}
				}
			}
			}while(!(modifPrenom.equals("O")) && (!(modifPrenom.equals("N"))));
		
		String modifDateNaissance;
		
		do {
			System.out.print("Voulez vous modifier la date de naissance ? oui ('O') ou non ('N') : ");
			modifDateNaissance = sc.nextLine();
			
			
			if (modifDateNaissance.equals("O")){
			
			System.out.println("Saisissir la valeur a remplacer :");
			  String dateNaissanceRemplace= sc.nextLine();
				
				for (int i = 0;i<clients[0].length;i++){
			
				if (clients [0][i].equals(codeClientModifier)){
				clients[3][i] = dateNaissanceRemplace;
			}
				}
			}
			}while(!(modifDateNaissance.equals("O")) && (!(modifDateNaissance.equals("N"))));	
			
			
			String modifEmail;
			boolean verifEmail;
		do {
			System.out.print("Voulez vous modifier le mail ? oui ('O') ou non ('N') : ");
			modifEmail = sc.nextLine();
			
			
			if (modifEmail.equals("O")){
				do{
					verifEmail = false;
					System.out.println("Saisissir la valeur a remplacer :");
					String emailRemplace= sc.nextLine();
					
					for(int i =0;i<clients[0].length;i++){
						if(emailRemplace.equals(clients[4][i])){
							verifEmail = true;
							break;
						}
					}
					if(verifEmail){
						System.out.println("cette adresse mail existe deja");
					} else{
						for(int j=0;j<clients[0].length;j++){
							if(clients[0][j].equals(codeClientModifier)){
								clients[4][j] = emailRemplace;
							}
						}
					}
				}while(verifEmail);	
			}
			}while(!(modifEmail.equals("O")) && (!(modifEmail.equals("N"))));
			return clients;		
	}
	/**
	*cette méthode permet la modification d'une agence;
	*
	*/
	public static String[][] modifAgenceGlobal(String[][] agences,Scanner sc){
		 String AgenceModifier= "";

		int indexModifier=-1;
		do{
			System.out.println("Veuillez saisir l_id de l_agence a modifier");
			AgenceModifier = sc.nextLine();
			
			for(int i=0;i<agences[0].length;i++){
				if(AgenceModifier.equals(agences[0][i])){
					indexModifier = i;
					break;
				}
			}
			if(indexModifier < 0){
				System.out.println("Ce compte Agence n_existe pas (veuillez saisir un code valable)");
			}									
		}while(indexModifier<0);
		
		String modifNomAgence;
		do {
			System.out.print("Voulez vous modifier le nom de l_agence ? oui ('O') ou non ('N') : ");
				modifNomAgence = sc.nextLine();
			
			
			if (modifNomAgence.equals("O")){
			
				System.out.println("Saisissir la valeur a remplacer :");
				String nomAgenceRemplace = sc.nextLine();
				
				for (int i = 0;i<agences[0].length;i++){
			
				if (agences[0][i].equals(AgenceModifier)){
				agences[1][i] = nomAgenceRemplace;
			}
				}
			}
			}while(!(modifNomAgence.equals("O")) && (!(modifNomAgence.equals("N"))));

		String modifAdressAgence;
		do {
			System.out.print("Voulez vous modifier l_adresse de l_agence ? oui ('O') ou non ('N') : ");
				modifAdressAgence = sc.nextLine();
			
			
			if (modifAdressAgence.equals("O")){
			
				System.out.println("Saisissir la valeur a remplacer :");
				String adressAgenceRemplace = sc.nextLine();
				
				for (int i = 0;i<agences[0].length;i++){
			
				if (agences[0][i].equals(AgenceModifier)){
				agences[2][i] = adressAgenceRemplace;
			}
				}
			}
			}while(!(modifNomAgence.equals("O")) && (!(modifNomAgence.equals("N"))));

		
		
			
			return agences;		
	}
	/**
	*cette méthode permet la modification d'un compte;
	*
	*/
	public static String[][] modifCompteGlobal(String[][] comptes,Scanner sc){
		 String compteModifier= "";

		 int indexModifier=-1;
		do{
			System.out.println("Veuillez saisir l_id du compte a modifier");
		   compteModifier = sc.nextLine();
			
			for(int i=0;i<comptes[0].length;i++){
				if(compteModifier.equals(comptes[0][i])){
					indexModifier = i;
					break;
				}
			}
			if(indexModifier < 0){
				System.out.println("Ce compte  n_existe pas (veuillez saisir un code valable)");
			}									
		}while(indexModifier<0);
		
		String modifSoldeCompte;
		do {
			System.out.print("Voulez vous modifier le solde du compte ? oui ('O') ou non ('N') : ");
				modifSoldeCompte = sc.nextLine();
			
			
			if (modifSoldeCompte.equals("O")){
			
				System.out.println("Saisissir la valeur a remplacer :");
				String soldeCompteRemplace = sc.nextLine();
				
				for (int i = 0;i<comptes[0].length;i++){
			
				if (comptes[0][i].equals(compteModifier)){
				comptes[4][i] = soldeCompteRemplace;
			}
				}
			}
			}while(!(modifSoldeCompte.equals("O")) && (!(modifSoldeCompte.equals("N"))));

		String modifDecouvertCompte;
		do {
			System.out.print("Voulez vous modifier le decouvert du compte? oui ('O') ou non ('N') : ");
				modifDecouvertCompte = sc.nextLine();
			
			
			if (modifDecouvertCompte.equals("O")){
				
				for (int i = 0;i<comptes[0].length;i++){
			
				if (comptes[0][i].equals(compteModifier)){
					if(comptes[5][i].equals("O")){
						comptes[5][i] = "N";
					}
					if(comptes[5][i].equals("N")){
						comptes[5][i] = "O";
					}
			}
				}
			}
			}while(!(modifDecouvertCompte.equals("O")) && (!(modifDecouvertCompte.equals("N"))));
			
		String modifTypeCompte;
		String typeModif;
		boolean verif = false;
		do {
			System.out.print("Voulez vous modifier le type du compte? oui ('O') ou non ('N') : ");
				modifTypeCompte = sc.nextLine();
			
			
			if (modifTypeCompte.equals("O")){
				do{
					do{
					System.out.println("Veuillez saisir le type de compte ('CC':Compte Courant,'LA':Livret A,'PEL':epargne logement.) : ");
					typeModif = sc.nextLine();
				}while(!(typeModif.equals("CC")) && !(typeModif.equals("LA")) && !(typeModif.equals("PEL")) );
				
				for (int i = 0;i<comptes[0].length;i++){
					if(comptes[0][i].equals(compteModifier)){
						verif = verifTypeCompte(typeModif,comptes,comptes[2][i]);
					}
				}
				if(verif){
					System.out.println("LE client possède deja un compte de ce type");
				} else{
					for (int i = 0;i<comptes[0].length;i++){
						if(comptes[0][i].equals(compteModifier)){
							comptes[3][i] = typeModif;
						}
					}
				}
				}while(verif);
				
			}
			}while(!(modifTypeCompte.equals("O")) && (!(modifTypeCompte.equals("N"))));
			return comptes;		
	}
	/**
	*cette méthode permet de mettre un tableau dans un csv;
	*
	*/
	public static void pushToCsv(String[][] tab, File file) throws Exception,IOException{
		FileWriter fw = new FileWriter(file);
		if(file.getName().equals("agences.csv")){
			for(int j = 0;j<tab[0].length;j++){
				fw.append(tab[0][j]+","+tab[1][j]+","+tab[2][j]+"\n");
			}
		}else if(file.getName().equals("clients.csv")){
			for(int j = 0;j<tab[0].length;j++){
				fw.append(tab[0][j]+","+tab[1][j]+","+tab[2][j]+","+tab[3][j]+","+tab[4][j]+"\n");
			}
		}else if(file.getName().equals("comptes.csv")){
			for(int j = 0;j<tab[0].length;j++){
				fw.append(tab[0][j]+","+tab[1][j]+","+tab[2][j]+","+tab[3][j]+","+tab[4][j]+","+tab[5][j]+"\n");
			}
		}
		fw.close();
	}
	/**
	*cette permet de supprimer un clients;
	*
	*/
	public static String[][] deleteClient(String[][] clients, Scanner sc,String codeClientSupp,int indexSupp){

		
		String clientsSupp[][] = new String[5][clients[0].length-1];
		
		if(clients[0].length > 1){
			for(int i=0;i<clients.length;i++){
				for(int j =0;j<clients[0].length-1;j++){
					if(j >= indexSupp){
						clientsSupp [i][j] = clients[i][j+1];
					} else{
						clientsSupp [i][j] = clients[i][j];
					}
				}
			}
		}
		return clientsSupp;
	}
	/**
	*cette methode permet de supprimer les comptes d'un clients spécifique;
	*
	*/
	public static String[][] deleteCompteOfClient(String[][] comptes, Scanner sc,String[][] clientsSupp,String codeClientSupp){
		int[] codeCompteSupp = new int[3];
		for(int i=0;i<codeCompteSupp.length;i++){
			codeCompteSupp[i] = -1;
		}
		int cpt =0;
		for(int j = 0;j<comptes[0].length;j++){
			if(codeClientSupp.equals(comptes[2][j])){
				cpt=0;
				for(int i =0;i<codeCompteSupp.length;i++){
					if(codeCompteSupp[i] != -1){
						cpt++;
					}
				}
				codeCompteSupp[cpt] = j;
			}
		}
		cpt=0;
		for(int i =0;i<codeCompteSupp.length;i++){
			if(codeCompteSupp[i] != -1){
				cpt++;
			}
		}
		
		String[][] comptesBis = new String[6][comptes[0].length-cpt];
		if(cpt == 1 && codeCompteSupp[0] == 0){
			for(int i=0;i<comptes.length;i++){
				for(int j = 1;j<comptes[0].length;j++){
					System.out.println(comptes[i][j]);
					comptesBis[i][j-1] = comptes[i][j];
				}
			}
		}else{
			cpt = 0;
			for(int i=0;i<comptesBis.length;i++){
				for(int j = 0;j<comptesBis[0].length-cpt;j++){
					if(j == codeCompteSupp[0] || j == codeCompteSupp[1] || j == codeCompteSupp[2]){
						cpt++;
					}
					comptesBis[i][j] = comptes[i][j+cpt];
				}
			}
		}

		
		return comptesBis;
	}
	/**
	*cette méthode permet la suppression d'une agence;
	*
	*/
	public static String[][] deleteAgence(String[][] agences, Scanner sc,String[][] comptes, String codeAgenceSupp, int indexSuppA){
		String agencesSupp[][] = new String[5][agences[0].length-1];
		
		if(agences[0].length > 1){
			for(int i=0;i<agences.length;i++){
				for(int j =0;j<agences[0].length-1;j++){
					if(j >= indexSuppA){
						agencesSupp [i][j] = agences[i][j+1];
					} else{
						agencesSupp [i][j] = agences[i][j];
					}
				}
			}
		}
		return agencesSupp;
	}
	/**
	*Permet la modification d'un id agence en fonction d'une agence supprimer;
	*
	*/
	public static String[][] modifCompte(String comptes[][],String codeAgenceSupp){
		for(int i =0;i<comptes[0].length;i++){
			if(comptes[1][i].equals(codeAgenceSupp)){
				comptes[1][i] = "99";
			}
		}
		return comptes;
	}
	/**
	* Cette methode permet de supprimer un comptes bancaire;
	*
	*/
	public static String[][] deleteCompte(String[][] comptes, Scanner sc){
		String codeCompteSuppC;
		int indexSuppC = -1;
		do{
			System.out.println("Veuillez saisir l_id du compte a supprimmer");
			codeCompteSuppC = sc.nextLine();
			for(int i=0;i<comptes[0].length;i++){
				if(codeCompteSuppC.equals(comptes[0][i])){
					indexSuppC = i;
				}
			}
			if(indexSuppC < 0){
				System.out.println("Ce compte n_existe pas (veuillez saisir un code valable)");
			}									
		}while(indexSuppC<0);
		
		String comptesSupp[][] = new String[6][comptes[0].length-1];
		
		if(comptes[0].length > 1){
			for(int i=0;i<comptes.length;i++){
				for(int j =0;j<comptes[0].length-1;j++){
					if(j >= indexSuppC){
						comptesSupp [i][j] = comptes[i][j+1];
					} else{
						comptesSupp [i][j] = comptes[i][j];
					}
				}
			}
		}
		return comptesSupp;
	}
	/**
	* Cette methode permet d'afficher tous les clients;
	*
	*/
	public static void displayClient(String[][] clients){
		if(clients[0].length == 1){
			System.out.println("Il n_y as aucun clients dans votre banque");
		}else{
			for(int i = 0;i<clients[0].length;i++){
				if(clients[0][i].equals("codeClient")){
					continue;
				}else{
					System.out.println("CODE CLIENT : "+clients[0][i]);
					System.out.println("NOM : "+clients[1][i]);
					System.out.println("PRENOM : "+clients[2][i]);
					System.out.println("DATE DE NAISSANCE : "+clients[3][i]);
					System.out.println("EMAIL : "+clients[4][i]+"\n");
			}
			
			}
		}
	}
	/**
	* Cette methode permet de rechercher un client selon son id;
	*
	*/
	public static void searchClient(String[][] clients, Scanner sc){
		System.out.println("Veuillez saisir votre recherche");
		String recherche = sc.nextLine();
		for(int i =0;i<clients[0].length;i++){
			if(clients[0][i].contains(recherche) || clients[1][i].contains(recherche) || clients[2][i].contains(recherche) || clients[3][i].contains(recherche) || clients[4][i].contains(recherche) ){
				System.out.println("CODE CLIENT : "+clients[0][i]);
				System.out.println("NOM : "+clients[1][i]);
				System.out.println("PRENOM : "+clients[2][i]);
				System.out.println("DATE DE NAISSANCE : "+clients[3][i]);
				System.out.println("EMAIL : "+clients[4][i]+"\n");										
			}
		}
	}
	/**
	* Cette methode permet d'imprimer les infos clients dans un txt;
	*
	*/
	public static void printClient(String[][] clients, String[][] comptes,Scanner sc) throws Exception,IOException{
		Path cheminTxt = Path.of("./txt/client.txt");
		File fichierTxt = cheminTxt.toFile();
		FileWriter fichierWriterTxt = new FileWriter(fichierTxt);
		int indexImprim = -1;
		String idImprim;
		do{
			System.out.println("saisir le code client a imprimer");
			idImprim = sc.nextLine().trim();
			for(int i =0;i<clients[0].length;i++){
				if(idImprim.equals(clients[0][i])){
					indexImprim = i;
					break;
				}
			}
			
		}while(indexImprim == -1);
		
		
		fichierWriterTxt.append("FICHE CLIENT \n \n");
		fichierWriterTxt.append("Numéro Client : "+clients[0][indexImprim]+"\n");
		fichierWriterTxt.append("Nom : "+clients[1][indexImprim]+"\n");
		fichierWriterTxt.append("Prénom :"+clients[2][indexImprim]+" \n");
		fichierWriterTxt.append("Date de naissance : "+clients[3][indexImprim]+"\n");
		fichierWriterTxt.append("\n");
		fichierWriterTxt.append("Liste de comptes \n\n");
		fichierWriterTxt.append("Numéro de compte \t\t\t\t\t\t Solde \n\n");
		for(int i =0;i<comptes[0].length;i++){
			if(idImprim.equals(comptes[2][i])){
				fichierWriterTxt.append(comptes[0][i]+" \t\t\t\t\t\t\t "+comptes[4][i]+" euros \n");
			}
		}
		fichierWriterTxt.close();
	}
	/**
	* Cette methode permet de demander les information pour la création d'agence;
	*
	*/
	public static String[][] askAgence(Scanner sc,String[][] agences,Random rand){
		boolean verifNomAdresse = true;
		String nom;
		String adresse;
		do{		
			verifNomAdresse = true;
			System.out.print("Veuillez saisir le nom de l_agence : ");
			nom = sc.nextLine().trim();
			System.out.print("Veuillez saisir l_adresse de l_agence : ");
			adresse = sc.nextLine().trim();
			
			int j = 0;
			while(j < agences[0].length){
				if(nom.equals(agences[NOM_AGENCES][j]) && adresse.equals(agences[ADRESSE_AGENCES][j])){
					verifNomAdresse = false;
					System.out.println("Cette agence existe deja : RECOMMENCER");
				}
				if((nom.equals("") || adresse.equals(""))){
					verifNomAdresse = false;
					System.out.println("Attention veuillez remplir tous les champs de texte !");
				}
				j++;
			}
										
		}while(!(verifNomAdresse));
		
		return agences = createAgence(agences,nom,adresse,rand);
	}
	/**
	* Cette methode permet de créer une agence;
	*
	*/
	public static String[][] createAgence(String[][] agences, String nom, String adresse, Random rand){
		String[][] newTab = new String[3][agences[0].length+1];
		
		if(agences[0].length > 0){
			for(int i =0;i<agences.length;i++){
				for(int j =0;j<agences[i].length;j++){
					newTab[i][j] = agences[i][j];
				}
			}
		}
		
		int codeAgence = codeAgenceRandom(agences,rand);
		
		newTab[ID_AGENCES][agences[0].length] = String.valueOf(codeAgence);
		newTab[NOM_AGENCES][agences[0].length] = nom;
		newTab[ADRESSE_AGENCES][agences[0].length] = adresse;
								
		return newTab;		
	}
	/**
	* Cette methode permet de récupérer les données d'un csv et de les mettres dans un tableau a deux dimensions;
	*
	*/
	public static String[][] pushCsvToTab(File file,int taille) throws Exception,IOException{
		Scanner scanner = new Scanner(file);
		String[] lineAgence;
		String[][] newTab = new String[taille][0];
		String[][] sauvegardeAgences = new String[taille][0];
		int cpt = -1;
		 
		while(scanner.hasNextLine()){
			cpt = cpt+1;
		
			newTab = new String[taille][cpt+1];
			if(sauvegardeAgences[0].length > 0){
				for(int i=0;i<sauvegardeAgences.length;i++){
					for(int j=0;j<sauvegardeAgences[i].length;j++){
						newTab[i][j] = sauvegardeAgences[i][j];
					}
				}
			}
			lineAgence = scanner.nextLine().split(",");
			
			for(int i =0;i<lineAgence.length;i++){
				newTab[i][cpt] = lineAgence[i];
			}
			
			sauvegardeAgences = newTab;
		}
		
		return sauvegardeAgences;
	}
	/**
	* Cette methode permet d'initialiser le csv agences;
	*
	*/
	public static String[][] initFileCsvAgence(File file,String[][] agences) throws Exception,IOException{
		String[][] newTab = new String[3][2];
			
		newTab[ID_AGENCES][agences[0].length] = "id";
		newTab[NOM_AGENCES][agences[0].length] = "nom";
		newTab[ADRESSE_AGENCES][agences[0].length] = "adresse";
		
		newTab[ID_AGENCES][agences[0].length+1] = "99";
		newTab[NOM_AGENCES][agences[0].length+1] = "GENERIQUE";
		newTab[ADRESSE_AGENCES][agences[0].length+1] = "INCONNUE";
		
		FileWriter fw = new FileWriter(file);
		file.createNewFile();
		for(int i=0;i<newTab[0].length;i++){
			fw.append(newTab[0][i]+","+newTab[1][i]+","+newTab[2][i]+"\n");
		}
		fw.close();					
		return newTab;		
	}
	/**
	* Cette methode permet d'initialiser le csv comptes;
	*
	*/
	public static String[][] initFileCsvCompte(File file,String[][] comptes) throws Exception,IOException{
		String[][] newTab = new String[6][1];
			
		newTab[ID_COMPTES][comptes[0].length] = "idCompte";
		newTab[CODE_AGENCES_COMPTES][comptes[0].length] = "idAgence";
		newTab[ID_CLIENTS_COMPTES][comptes[0].length] = "idClient";
		newTab[TYPE_COMPTES][comptes[0].length] = "type";
		newTab[SOLDE_COMPTES][comptes[0].length] = "solde";
		newTab[DECOUVERT_COMPTES][comptes[0].length] = "decouvert";
		
		
		FileWriter fw = new FileWriter(file);
		file.createNewFile();
		fw.append(newTab[0][0]+","+newTab[1][0]+","+newTab[2][0]+","+newTab[3][0]+","+newTab[4][0]+","+newTab[5][0]+"\n");
		fw.close();					
		return newTab;		
	}
	/**
	* Cette methode permet d'initialiser le csv clients;
	*
	*/
	public static String[][] initFileCsvClient(File file,String[][] clients) throws Exception,IOException{
		String[][] newTab = new String[5][1];
			
		newTab[0][clients[0].length] = "codeClient" ; 
		newTab[1][clients[0].length] = "nom";
		newTab[2][clients[0].length] = "prenom";
		newTab[3][clients[0].length] = "dateNaissance";
		newTab[4][clients[0].length] = "email";
		
		
		
		FileWriter fw = new FileWriter(file);
		file.createNewFile();
		fw.append(newTab[0][0]+","+newTab[1][0]+","+newTab[2][0]+","+newTab[3][0]+","+newTab[4][0]+"\n");
		fw.close();					
		return newTab;		
	}
	
	/**
	* Cette methode permet de créer un code d'agence aléatoirement;
	*
	*/
	public static int codeAgenceRandom(String[][] agences, Random rand){
		int codeRandomAgence;
		boolean verif = true;
		do{
			codeRandomAgence = rand.nextInt(900)+100;
			for(int j=0; j<agences[0].length;j++){
				if(String.valueOf(codeRandomAgence).equals(agences[ID_AGENCES][j])){
					verif = false;
					break;
				}
			}														
		}while(!(verif));
		
		return codeRandomAgence;
	}
	
	/**
	* Cette methode permet de demander les infos pour la création de comptes;
	*
	*/
	public static String[][] askCompte(Scanner sc,String[][] comptes,String[][] agences,Random rand,String[][] clients){
		boolean verif;
		String idClient;
		String idAgences;
		String type;
		String decouvert;
		String solde;
								

		do{
			verif = false;
			System.out.print("Veuillez saisir l_id du client : ");
			idClient = sc.nextLine().trim();
			for(int i=0;i<clients[0].length;i++){
				if(idClient.equals(clients[ID_AGENCES][i])){
					verif = true;
					break;
				}
			}
			if(!(verif)){
				System.out.println("l_id client saisie n_existe pas");
			}			
		}while(!(verif) || idClient.equals(""));
		
		
		do{
			verif = false;
			System.out.print("Veuillez saisir l_id de l_agence : ");
			idAgences = sc.nextLine().trim();
				
			for(int i=0;i<agences[0].length;i++){
				if(idAgences.equals(agences[ID_AGENCES][i])){
					verif = true;
					break;
				}
			}
			if(!(verif)){
				System.out.println("l_id agences saisie n_existe pas ! (appuyer sur entrez pour continuer )");
				sc.nextLine();
			}	
		}while(!(verif) || idAgences.equals(""));
		
		boolean verifType = false;
		do{
			System.out.print("Veuillez saisir le type de compte ('CC':Compte Courant,'LA':Livret A,'PEL':epargne logement.) : ");
			type = sc.nextLine().trim();	
			if(!(type.equals("CC")) && !(type.equals("LA")) && !(type.equals("PEL"))){
				System.out.print("Le type saisie n_existe pas ! (appuyer sur entrez pour continuer )");
				sc.nextLine();
			} else{
				verifType = verifTypeCompte(type,comptes,idClient);	
				if (verifType){
					System.out.println("Le client possède déja un compte de ce type (Appuyer sur entrer pour continuer)");
					sc.nextLine();
				}
			}
			

			
		}while((!(type.equals("CC")) && !(type.equals("LA")) && !(type.equals("PEL"))) || (verifType) || type.equals(""));

		do{
			System.out.print("Veuillez saisir si le decouvert est autoriser ('O') ou non ('N') : ");
			decouvert = sc.nextLine().trim();	
			if(!(decouvert.equals("O")) && !(decouvert.equals("N"))){
				System.out.print("La saisie est incorrect ! (appuyer sur entrez pour continuer )");
				sc.nextLine();
			}
		}while(!(decouvert.equals("O")) && !(decouvert.equals("N")) || decouvert.equals(""));

		solde = "00.0";
		
		return comptes = createCompte(comptes,rand,idClient,idAgences,type,decouvert,solde);
	}
	/**
	*
	*Cette methode permet de savoir si un client à déja un certain type de comptes
	*
	*/
	public static boolean verifTypeCompte(String type,String comptes[][],String idClient){
		for(int i=0;i<comptes[0].length;i++){
			if(type.equals(comptes[TYPE_COMPTES][i]) && idClient.equals(comptes[ID_CLIENTS_COMPTES][i])){
				return true;
			}
		}
		return false;
	}
	/**
	* Cette methode permet de créer un compte;
	*
	*/
	public static String[][] createCompte(String[][] comptes,Random rand,String idClient, String idAgences,String type, String decouvert, String solde){
		String[][] newTab = new String[6][comptes[0].length+1];
		
		if(comptes[0].length > 0){
			for(int i =0;i<comptes.length;i++){
				for(int j =0;j<comptes[i].length;j++){
					newTab[i][j] = comptes[i][j];
				}
			}
		}
		
		long codeCompte = codeCompteRandom(comptes,rand);
		
		newTab[ID_COMPTES][comptes[0].length] = String.valueOf(codeCompte);
		newTab[CODE_AGENCES_COMPTES][comptes[0].length] = idAgences;
		newTab[ID_CLIENTS_COMPTES][comptes[0].length] = idClient;
		newTab[TYPE_COMPTES][comptes[0].length] = type;
		newTab[SOLDE_COMPTES][comptes[0].length] = solde;
		newTab[DECOUVERT_COMPTES][comptes[0].length] = decouvert;
								
		return newTab;		
	}
	/**
	* Cette methode permet de générer un code comptes aléatoire à 11 chiffres;
	*
	*/
	public static long codeCompteRandom(String[][] comptes, Random rand){
		long codeRandomCompte;
		boolean verif = true;
		do{
			codeRandomCompte = rand.nextInt(1000000000) + (rand.nextInt(90) + 10) * 1000000000L;
			for(int j=0; j<comptes[0].length;j++){
				if(String.valueOf(codeRandomCompte).equals(comptes[ID_COMPTES][j])){
					verif = false;
					break;
				}
			}														
		}while(!(verif));
		
		return codeRandomCompte;
	}
	/**
	* Cette methode permet d'afficher toutes les agences;
	*
	*/
	public static void displayAgences(Scanner sc,String agences[][]){
		if(agences[0].length == 0){
			System.out.println("Il n_y as aucune agences dans votre banque (appuyez sur entrer pour continuer)");
			sc.nextLine();
		} else{
			for(int j = 0;j<agences[0].length;j++){
				if(agences[ID_AGENCES][j].equals("id")){
					continue;
				} else{
					System.out.println("ID_AGENCES : "+agences[ID_AGENCES][j]+"\nNOM :"+agences[NOM_AGENCES][j]+"\nSITUE A :"+agences[ADRESSE_AGENCES][j]+"\n");
				}
				
			}
		}
	}
	
	/**
	* Cette methode permet d'afficher tous les comptes;
	*
	*/
	public static void displayComptes(Scanner sc,String comptes[][]){
		if(comptes[0].length == 1){
			System.out.println("Il n_y as aucun compte clients dans votre banque (appuyez sur entrer pour continuer)");
			sc.nextLine();
		} else{
			for(int j = 0;j<comptes[0].length;j++){
				if(comptes[ID_COMPTES][j].equals("idCompte")){
					continue;
				} else{
					affichageCompte(comptes,j);
				}
				
			}
		}
	}
	
	/**
	* Cette methode permet d'afficher tous les comptes d'un client;
	*
	*/
	public static void displayComptes(Scanner sc,String comptes[][],String idClient){
		int cpt = 0;
		if(comptes[0].length == 1){
			System.out.println("Il n_y as aucun compte clients dans votre banque (appuyez sur entrer pour continuer)");
			sc.nextLine();
		} else{
			for(int j = 0;j<comptes[0].length;j++){
				if(idClient.equals(comptes[ID_CLIENTS_COMPTES][j])){
					cpt++;
					affichageCompte(comptes,j);
				}
			}
			
			if(cpt == 0){
				System.out.print("Ce client ne possede aucun compte");
			}
			System.out.println("Appuyez sur entrer pour continuer ");
			sc.nextLine();
		}
	}
	/**
	*
	*Cette méthode permet le formatage de l'afficha des comptes;
	*
	*/
	public static void affichageCompte(String[][] comptes,int j){
		System.out.print("ID :"+comptes[ID_COMPTES][j]);
		if(comptes[TYPE_COMPTES][j].equals("CC")){
			System.out.print(" TYPE : Compte Courant");
		}else if(comptes[TYPE_COMPTES][j].equals("LA")){
			System.out.print(" TYPE : Livret A");
		}else if(comptes[TYPE_COMPTES][j].equals("PEL")){
			System.out.print(" TYPE : Plan Epargne Logement");
		}
					
		System.out.print("\nSOLDE : "+comptes[SOLDE_COMPTES][j]+"\nDans l_agence : "+comptes[CODE_AGENCES_COMPTES][j]+"\nAppartient au client numero : "+comptes[ID_CLIENTS_COMPTES][j]+"\n");
		if(comptes[DECOUVERT_COMPTES][j].equals("O")){
			System.out.println("Avec decouvert autoriser\n\n");
		} else {
			System.out.println("Sans decouvert autoriser\n\n");
		}
	}
	/**
	*
	* Cette methode permet de faire une recherche de compte en banque en fonction de son id;
	*
	*/
	public static void rechercheCompte(Scanner sc,String[][] comptes){
		System.out.println("Veuillez saisir l'id du compte ");
		String id = sc.nextLine();
		for(int i =0;i< comptes[0].length;i++){
			if(comptes[ID_COMPTES][i].contains(id)){
				affichageCompte(comptes,i);
		} else{
			System.out.println("la recherche faites n'as pas aboutie");
		}
		}
	}
	/**
	*
	* Cette méthode permet de créer un client;
	*
	*/
	public static String[][] createClient(String[][] clients,Scanner sc,Random rand){
		String clientsBis [] [] = new String [5] [clients[0].length+1];
		
		if ( clients[0].length >0 ){
			for (int i=0 ; i<clients.length; i++){
				for (int j=0 ; j<clients[i].length; j++){
					clientsBis [i][j] = clients [i] [j] ;
				}
			}
		}
		boolean verifCode;
		String codeClient;
		do{
			verifCode =false;
			int randomSix = rand.nextInt(1000000-100000)+100000;
			char car1 = (char)('a'+rand.nextInt(26));
			char car2 = (char)('a'+rand.nextInt(26));
			codeClient = (String.valueOf(car1)+String.valueOf(car2)+String.valueOf(randomSix)).toUpperCase();
			for(int i =0;i<clients[0].length;i++){
				if(codeClient.equals(clients[0][i])){
					verifCode = true;
					break;
				}
			}
		}while(verifCode);
		String nom;
		String prenom;
		String dateNaissance;
		do{
			System.out.println("saisissez le nom du client	");
			nom = sc.nextLine();
			System.out.println("saisissez le prenom du client");
			prenom =sc.nextLine();
			System.out.println("saisissez la date de naissance ");
			dateNaissance = sc.nextLine();
			if(nom.equals("") || prenom.equals("") || dateNaissance.equals("") ){
				System.out.println("Une des informations n_as pas etait donner");
			}
		}while(nom.equals("") || prenom.equals("") || dateNaissance.equals("") );
								
		boolean verifEmail;
		String email;
		do {
			verifEmail = false;
			System.out.println("saisissez email ");
			email =sc.nextLine();
									
			for (int i = 0;i<clients[0].length;i++ ){
				if (email.equals(clients[4][i]) ){
					verifEmail = true ; 
					break;
				}
			}
		}while (verifEmail);
						
		clientsBis [0][clients[0].length] = codeClient ; 
		clientsBis [1][clients[0].length] = nom;
		clientsBis [2][clients[0].length] = prenom;
		clientsBis [3][clients[0].length] = dateNaissance;
		clientsBis [4][clients[0].length] = email;
								
		return clientsBis;		
	}
	
	
}